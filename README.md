# audio-drain
(APACHE v2.0) audio: processing pipe for simple agorithms (resampling, change format, reorder channel, volume ...) 1 flow

[![Build Status](https://travis-ci.org/musicdsp/audio-drain.svg?branch=master)](https://travis-ci.org/musicdsp/audio-drain)
